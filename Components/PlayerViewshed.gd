extends Node

onready var grid_mover = get_node("../GridMover")


func _ready():
  TurnManager.connect("update_player_viewshed", self, "handle_viewshed_update")


func handle_viewshed_update():
  VisibilityManager.update_player_visibility(grid_mover.grid_position, 20)

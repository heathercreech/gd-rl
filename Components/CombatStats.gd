extends Node
class_name CombatStats

signal take_damage(power, current_health, total_health)
signal died

var animation_player: AnimationPlayer
var parent

export var health_total = 10
export var health = 10
export var attack = 2
export var defense = 1
export (String) var display_name = ''


# Called when the node enters the scene tree for the first time.
func _ready():
  parent = get_parent()
  animation_player = parent.get_node_or_null("AnimationPlayer")


func take_damage(power: int):
  health -= power
  emit_signal("take_damage", power, health, health_total)
  print("{name}, Took {damage} points of damage".format({"damage": power, "name": display_name}))
  if health <= 0:
    _die()


func _die():
  emit_signal("died")
  print("Died!")

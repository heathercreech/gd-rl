extends Camera2D
class_name PlayerCamera

export(float) var drag_speed = 1.0
export(float) var zoom_increment_per_scroll = 0.1
export (float) var zoom_speed = 2.0
export(float) var max_zoom = 0.5
export(float) var min_zoom = 1.0

export(float) var soft_left_limit = 0
export(float) var soft_right_limit = 0
export(float) var soft_top_limit = 0
export(float) var soft_bottom_limit = 0
export(float) var hard_left_limit = -100
export(float) var hard_right_limit = -100
export(float) var hard_top_limit = -100
export(float) var hard_bottom_limit = -100
export(float) var soft_limit_spring_strength = 5

var is_dragging = false
var should_correct_against_soft_limit = false
var drag_start_position;
var target_zoom;


func _ready():
  target_zoom = zoom
  set_limits()
  GameWorld.connect("world_map_updated", self, "set_limits")

func set_limits():
  var world_rect = GameWorld.tile_map_rect
  soft_right_limit = (world_rect.size.x + world_rect.position.x) * GameWorld.cell_size.x - soft_right_limit
  soft_bottom_limit = (world_rect.size.y + world_rect.position.y) * GameWorld.cell_size.y - soft_bottom_limit
  limit_left = hard_left_limit + world_rect.position.x * GameWorld.cell_size.x
  limit_right = (world_rect.size.x + world_rect.position.x) * GameWorld.cell_size.x - hard_right_limit
  limit_top = hard_top_limit + world_rect.position.y * GameWorld.cell_size.y
  limit_bottom = (world_rect.size.y + world_rect.position.y) * GameWorld.cell_size.y - hard_bottom_limit



func _physics_process(delta):
  if is_dragging:
    var mouse_position = get_global_mouse_position()
    var drag_delta = drag_start_position - mouse_position
    if mouse_position != drag_start_position:
      var next_position = position + drag_delta * drag_speed
      var next_global_position = global_position + drag_delta * drag_speed
      var viewport_offset = get_viewport_rect().size * zoom / 2
      if next_global_position.x > limit_left + viewport_offset.x and next_global_position.x + viewport_offset.x < limit_right:
        position.x = next_position.x
      if next_global_position.y > limit_top + viewport_offset.y and next_global_position.y + viewport_offset.y < limit_bottom:
        position.y = next_position.y
      drag_start_position = mouse_position

  _spring_top_left_to_soft_limit(delta)
  _spring_bottom_right_to_soft_limit(delta)

  if target_zoom != zoom:
    zoom += (target_zoom - zoom) * delta * zoom_speed

func _input(event):
  if event.is_action_pressed("drag"):
    drag_start_position = get_global_mouse_position()
    _disable_drag_margins()
    is_dragging = true
  elif event.is_action_released("drag"):
    is_dragging = false
  elif event.is_action("zoom_in"):
    if target_zoom > Vector2(max_zoom, max_zoom):
      target_zoom -= Vector2(zoom_increment_per_scroll, zoom_increment_per_scroll)
  elif event.is_action("zoom_out"):
    if target_zoom < Vector2(min_zoom, min_zoom):
      target_zoom += Vector2(zoom_increment_per_scroll, zoom_increment_per_scroll)
  elif event.is_action("center_camera_on_player"):
    position = Vector2.ZERO
    _enable_drag_margins()

func _disable_drag_margins():
  drag_margin_h_enabled = false
  drag_margin_v_enabled = false

func _enable_drag_margins():
  drag_margin_h_enabled = true
  drag_margin_v_enabled = true

func _get_top_left_position():
  return global_position - get_viewport_rect().size * zoom / 2

func _spring_top_left_to_soft_limit(delta):
  var top_left_position = _get_top_left_position()
  if (top_left_position.x < soft_left_limit or top_left_position.y < soft_top_limit) and !is_dragging:
    _disable_drag_margins()
    if top_left_position.x < soft_left_limit:
      position += (Vector2(soft_left_limit, top_left_position.y)- top_left_position).normalized() * delta * soft_limit_spring_strength
    if top_left_position.y < soft_top_limit:
      position += (Vector2(top_left_position.x, soft_top_limit) - top_left_position).normalized() * delta * soft_limit_spring_strength

    if top_left_position.x >= soft_left_limit and top_left_position.y >= soft_top_limit:
      _enable_drag_margins()

func _get_bottom_right_position():
  return global_position + get_viewport_rect().size * zoom / 2

func _spring_bottom_right_to_soft_limit(delta):
  var bottom_right_position = _get_bottom_right_position()
  if (bottom_right_position.x > soft_right_limit or bottom_right_position.y > soft_bottom_limit) and !is_dragging:
    _disable_drag_margins()
    if bottom_right_position.x > soft_right_limit:
      position += (Vector2(soft_right_limit, bottom_right_position.y) - bottom_right_position).normalized() * delta * soft_limit_spring_strength
    if bottom_right_position.y > soft_bottom_limit:
      position += (Vector2(bottom_right_position.x, soft_bottom_limit) - bottom_right_position).normalized() * delta * soft_limit_spring_strength

    if bottom_right_position.x <= soft_right_limit and bottom_right_position.y <= soft_bottom_limit:
      _enable_drag_margins()

extends Node2D

signal inspect_succeeded
signal inspect_failed

func _ready():
  # We don't want to hijacking input right away, other actions could be the default
  disable()

func disable():
  set_process(false)
  set_process_input(false)
  set_process_unhandled_input(false)
  Overlay.clear()

func enable():
  set_process(true)
  set_process_input(true)
  set_process_unhandled_input(true)

func _process(_delta):
  var mouse_grid_position = GameWorld.world_to_grid(get_global_mouse_position())
  if VisibilityManager.get_cell_visibility(mouse_grid_position) in [MapHelpers.VisibilityLevel.Seen, MapHelpers.VisibilityLevel.Visible]:
    # Set to the movement indicator tile id
    Overlay.clear()
    Overlay.set_cellv(mouse_grid_position, Overlay.OverlayTile.Scan)

func _unhandled_input(event):
  if event.is_action_released("pick_on_screen"):
    var mouse_grid_position = GameWorld.world_to_grid(get_global_mouse_position())
    var blocker_info = MovementBlockerManager.get_blocker(mouse_grid_position)
    if blocker_info[0] == Blockers.BlockerType.Entity:
      var entity = blocker_info[1]
      if entity and entity.has_method("scanned"):
        entity.scanned()
        TurnManager.go_to_next_turn_step()
        emit_signal("inspect_succeeded")
        return
    emit_signal("inspect_failed")

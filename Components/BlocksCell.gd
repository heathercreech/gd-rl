extends Node2D

var combat_stats: CombatStats
var blocking_root
var grid_mover

# warning-ignore:unused_signal
signal bumped(from_position)

# Called when the node enters the scene tree for the first time.
func _ready():
  blocking_root = get_parent()

  grid_mover = blocking_root.get_node("GridMover")
  grid_mover.connect("moved", self, "_on_GridMover_moved")
  grid_mover.connect("blocked", self, "_on_GridMover_blocked")

  MovementBlockerManager.block_tile(GameWorld.world_to_grid(blocking_root.position), blocking_root)

func _on_GridMover_moved(old_grid_position, new_grid_position):
  MovementBlockerManager.unblock_tile(old_grid_position)
  MovementBlockerManager.block_tile(new_grid_position, blocking_root)

func _on_GridMover_blocked(blocker_grid_position):
  var blocker_info = MovementBlockerManager.get_blocker(blocker_grid_position)
  if blocker_info[0] == Blockers.BlockerType.Entity:
    var blocks_cell_component = blocker_info[1].get_node_or_null("BlocksCell")
    if blocks_cell_component:
      blocks_cell_component.emit_signal("bumped", grid_mover.grid_position)


func _exit_tree():
  MovementBlockerManager.unblock_tile(grid_mover.grid_position)

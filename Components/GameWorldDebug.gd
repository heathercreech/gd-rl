extends Node2D



func _ready():
  pass

func _draw():
  """
  for point_id in PlayerAStarManager.a_star.get_points():
    var point = GameWorld.grid_to_world(PlayerAStarManager.a_star.get_point_position(point_id))
    draw_colored_polygon([
      point + Vector2(4, 4),
      point + Vector2(GameWorld.cell_size.x, 0) + Vector2(-4, 4),
      point + GameWorld.cell_size - Vector2(4, 4),
      point + Vector2(0, GameWorld.cell_size.y) + Vector2(4, -4)
     ], Color(0, 255, 0))
    for connected_point_id in PlayerAStarManager.a_star.get_point_connections(point_id):
      draw_line(point + GameWorld.cell_size / 2, GameWorld.grid_to_world(PlayerAStarManager.a_star.get_point_position(connected_point_id)) + GameWorld.cell_size / 2, Color(255, 0, 0), 1)
  """
  for cell in VisibilityManager.cells_between_debug:
    var point = GameWorld.grid_to_world(cell)
    draw_colored_polygon([
      point + Vector2(4, 4),
      point + Vector2(GameWorld.cell_size.x, 0) + Vector2(-4, 4),
      point + GameWorld.cell_size - Vector2(4, 4),
      point + Vector2(0, GameWorld.cell_size.y) + Vector2(4, -4)
     ], Color(0, 255, 0))

func _process(_delta):
  update()

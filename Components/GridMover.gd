extends Node2D
class_name GridMover

signal moved(old_grid_position, new_grid_position)
signal finished_move_animation
signal blocked(blocked_grid_position)

var movement_root: Node2D
var grid_position = Vector2(0, 0)
var start_position = null
var target_position = null
export(float) var animation_duration = .1
var elapsed_time = 0.0

func _ready():
  movement_root = get_parent()

  grid_position = GameWorld.world_to_grid(movement_root.position)
  movement_root.position = GameWorld.grid_to_world(grid_position) + GameWorld.cell_size / 2

func _physics_process(delta):
  if target_position != null and movement_root.position != target_position:
    elapsed_time += delta
    movement_root.position = start_position.linear_interpolate(target_position, elapsed_time / animation_duration)
    if movement_root.position == target_position:
      elapsed_time = 0
      start_position = null
      target_position = null
      emit_signal("finished_move_animation")


func move(grid_position_delta: Vector2):
  var new_grid_position = grid_position + grid_position_delta
  if !MovementBlockerManager.is_movement_blocked(new_grid_position):
    var old_grid_position = grid_position
    grid_position = new_grid_position
    start_position = movement_root.position
    target_position = GameWorld.grid_to_world(grid_position) + GameWorld.cell_size / 2
    emit_signal("moved", old_grid_position, grid_position)
  else:
    emit_signal("blocked", new_grid_position)


func move_left():
  move(Vector2(-1, 0))


func move_right():
  move(Vector2(1, 0))


func move_up():
  move(Vector2(0, -1))


func move_down():
  move(Vector2(0, 1))

extends Node2D
class_name PlayerMovement

onready var grid_mover: GridMover = get_node("../GridMover")

var path_cache: Array = []
var is_mouse_pressed: bool = false
var mouse_position_cache
var destination_cell


func _ready():
  grid_mover.connect("finished_move_animation", self, "_on_move_finished")


func disable():
  set_process(false)
  set_process_input(false)
  set_process_unhandled_input(false)


func enable():
  set_process(true)
  set_process_input(true)
  set_process_unhandled_input(true)


func _process(_delta):
  if is_mouse_pressed:
    var mouse_grid_position = GameWorld.world_to_grid(get_global_mouse_position())
    if mouse_position_cache != mouse_grid_position:
      mouse_position_cache = mouse_grid_position
      _clear_movement_indicators()
      var path = PlayerAStarManager.get_point_path(grid_mover.grid_position, mouse_position_cache)
      if path != null:
        path_cache = path
    # Only show the movement indicators if we're not about to blocked
    var blocker = MovementBlockerManager.get_blocker(mouse_position_cache)
    var mouse_distance_from_player = mouse_position_cache.distance_to(grid_mover.grid_position)
    if path_cache.size() > 1 and (mouse_distance_from_player >= 2 or blocker[0] == null):
      var actual_path = path_cache.slice(1, path_cache.size())
      for cell in actual_path:
        # And only add an overlay if the cell has been seen
        if VisibilityManager.get_cell_visibility(Vector2(cell[0], cell[1])) in [MapHelpers.VisibilityLevel.Seen, MapHelpers.VisibilityLevel.Visible]:
          # Set to the movement indicator tile id
          Overlay.set_cell(cell[0], cell[1], Overlay.OverlayTile.Move)


  # Travel to clicked destination unless we spot an enemy along the way
  if TurnManager.get_current_step() == TurnManager.TurnStep.PlayerTurn and destination_cell != null:
    # Refetch this to avoid any blockers that may have moved position
    var path = PlayerAStarManager.get_point_path(grid_mover.grid_position, destination_cell)
    var is_enemy_visible = false
    var visible_cells = VisibilityManager.get_used_cells_by_id(MapHelpers.VisibilityLevel.Visible)

    # TODO: Make this actually check for enemies instead of just movement blockers
    for cell in visible_cells:
      var blocker_info = MovementBlockerManager.get_blocker(cell)
      if blocker_info[0] == Blockers.BlockerType.Entity and blocker_info[1] != get_parent():
        if blocker_info[1].get_node_or_null("StopsPlayerMovementOnSight") != null:
          is_enemy_visible = true
        break
    if path.size() > 1:
      grid_mover.move(path[1] - grid_mover.grid_position)
    if grid_mover.grid_position == destination_cell or path.size() < 1 or is_enemy_visible:
      destination_cell = null
    TurnManager.set_is_waiting(true)

func _on_move_finished():
  TurnManager.go_to_next_turn_step()


func _clear_movement_indicators():
  Overlay.clear()


func _unhandled_input(event):
  if TurnManager.get_current_step() == TurnManager.TurnStep.PlayerTurn:
    _handle_player_turn_input(event)


func _handle_player_turn_input(event):
  var has_acted = false
  if event.is_action_pressed("ui_left"):
    grid_mover.move_left()
    has_acted = true
  elif event.is_action_pressed("ui_right"):
    grid_mover.move_right()
    has_acted = true
  elif event.is_action_pressed("ui_up"):
    grid_mover.move_up()
    has_acted = true
  elif event.is_action_pressed("ui_down"):
    grid_mover.move_down()
    has_acted = true
  elif event.is_action_pressed("pick_on_screen") or event.is_action_released("pick_on_screen"):
    var click_grid_position: Vector2 = GameWorld.world_to_grid(get_global_mouse_position())
    var path = PlayerAStarManager.get_point_path(grid_mover.grid_position, click_grid_position)
    if path != null:
      path_cache = path
      if event.is_pressed():
        is_mouse_pressed = true
      if not event.is_pressed():
        is_mouse_pressed = false
        var blocker = MovementBlockerManager.get_blocker(click_grid_position)
        var click_distance = click_grid_position.distance_to(grid_mover.grid_position)
        if blocker[0] != null and click_distance > 0 and click_distance < 2:
          # Bump the blocker
          grid_mover.move(click_grid_position - grid_mover.grid_position)
          has_acted = true
        elif path_cache.size() > 1:
          destination_cell = click_grid_position
        mouse_position_cache = null
        _clear_movement_indicators()
  if has_acted:
    TurnManager.go_to_next_turn_step()

extends Area2D


# In ms
export(String, MULTILINE) var text = ''
export var hover_time_until_tooltip_shows = 500
export var hover_time_until_locked = 500
export var lock_expire_time = 500
export var padding = 2
export var lock_border_width = 1
var label_wrapper
var label
var label_background
var lock_indicator_border
var timer


# Called when the node enters the scene tree for the first time.
func _ready():
  label_wrapper = $LabelWrapper
  label_background = $LabelWrapper/ColorRect
  lock_indicator_border = $LabelWrapper/BorderColorRect
  label = $LabelWrapper/Label
  label.text = text
  

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#  pass

func _start_timer(wait_time_in_ms, func_name):
  timer = Timer.new()
  add_child(timer)
  timer.autostart = true
  timer.wait_time = wait_time_in_ms / 1000.0
  timer.connect('timeout', self, func_name)
  timer.start()

func _cleanup_timer():
  if timer and is_instance_valid(timer):
    timer.queue_free()
  
func _start_initial_hover_timer():
  _start_timer(hover_time_until_tooltip_shows, "_initial_hover_timer_timeout")

func _initial_hover_timer_timeout():
  label_wrapper.visible = true
  _cleanup_timer()
  _start_pre_lock_hover_timer()
  
func _start_pre_lock_hover_timer():
  _start_timer(hover_time_until_locked, "_pre_lock_timer_timeout")

func _pre_lock_timer_timeout():
  lock_indicator_border.visible = true
  _cleanup_timer()

func _start_post_lock_hover_timer():
  _start_timer(lock_expire_time, "_post_lock_timer_timeout")

func _post_lock_timer_timeout():
  label_wrapper.visible = false
  lock_indicator_border.visible = false
  _cleanup_timer()

func _on_Tooltip_mouse_entered():
  _cleanup_timer()
  _start_initial_hover_timer()

func _on_Tooltip_mouse_exited():
  # Currently locked, let the lock expiry timer handle it
  if lock_indicator_border.visible:
    _start_post_lock_hover_timer()
  else:
    label_wrapper.visible = false
    _cleanup_timer()

func _on_Label_item_rect_changed():
  if label:
    label_background.rect_size = label.rect_size + Vector2(padding * 2, padding * 2)
    label_background.rect_position = Vector2(-padding, -label_background.rect_size.y / 2)
    lock_indicator_border.rect_size = label_background.rect_size + Vector2(lock_border_width * 2, lock_border_width * 2)
    lock_indicator_border.rect_position = Vector2(-lock_border_width - padding, -lock_indicator_border.rect_size.y / 2)

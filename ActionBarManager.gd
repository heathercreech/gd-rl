extends Node


onready var action_bar = $ActionBar
onready var player = get_node("../Player")
onready var player_movement = player.get_node("PlayerMovement")
onready var inspect_action = player.get_node("Actions/PlayerActionInspect")
onready var shoot_action = player.get_node("Actions/PlayerActionShoot")

var active_action = null

# Called when the node enters the scene tree for the first time.
func _ready():
  action_bar.register_action_with_slot(0, self, "_enable_inspect")
  action_bar.register_action_with_slot(1, self, "_enable_shoot")

  inspect_action.connect("inspect_succeeded", self, "_disable_inspect")
  inspect_action.connect("inspect_failed", self, "_disable_inspect")

  shoot_action.connect("action_taken", self, "_disable_shoot")

func _enable_inspect():
  _disable_active_action()
  active_action = inspect_action
  inspect_action.enable()
  player_movement.disable()

func _disable_inspect():
  inspect_action.disable()
  player_movement.enable()

func _enable_shoot():
  _disable_active_action()
  active_action = shoot_action
  shoot_action.enable()
  player_movement.disable()

func _disable_shoot():
  shoot_action.disable()
  player_movement.enable()

func _disable_active_action():
  if active_action:
    active_action.disable()

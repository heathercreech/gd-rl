extends Node

enum VisibilityLevel {
  Unseen = 0
  Seen = 1
  Visible = 2
}

class TileProperties:
  var blocks_movement = false
  var blocks_view = false

class TilePropertyFactory:
  var instance

  func _init():
    instance = TileProperties.new()

  func blocks_movement():
    instance.blocks_movement = true
    return self

  func blocks_view():
    instance.blocks_view = true
    return self

  func finish():
    return instance

var tile_property_map: Dictionary = {
  "wall": TilePropertyFactory.new().blocks_movement().blocks_view().finish(),
  "floor": TilePropertyFactory.new().finish(),
  "move indicator": TilePropertyFactory.new().finish()
}

func calc_astar_id(grid_position, tile_map_rect):
  return (grid_position.x + tile_map_rect.position.x) + (grid_position.y + tile_map_rect.position.y) * tile_map_rect.size.x

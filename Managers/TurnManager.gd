extends Node

signal activate_traps
signal update_player_viewshed
signal player_turn_setup
signal player_turn_cleanup
signal enemy_turn
signal enemy_turn_cleanup
signal determine_actor_visibility

enum TurnStep {
  UpdatePlayerViewshed
  ActivateTraps
  PlayerTurn
  PlayerTurnCleanup
  DetermineActorVisibility
  EnemyTurn
  EnemyTurnCleanup
  # A special turn step that signifies something is currently processing and we shouldn't move forward with anything
  Waiting
 }

var current_turn_step_index = 0
var set_up_turn_step_order = [
  TurnStep.UpdatePlayerViewshed,
  TurnStep.DetermineActorVisibility
]

var turn_step_order = [
  TurnStep.PlayerTurn,
  TurnStep.PlayerTurnCleanup,
  TurnStep.UpdatePlayerViewshed,
  TurnStep.DetermineActorVisibility,
  TurnStep.EnemyTurn,
  TurnStep.EnemyTurnCleanup,
  TurnStep.ActivateTraps,
]

var has_done_game_start_up_initialization = false
var has_done_set_up_for_current_step = false
var is_waiting = false

func get_current_step():
  if is_waiting:
    return TurnStep.Waiting
  return turn_step_order[current_turn_step_index]

func _set_turn_step(turn_step_index):
  current_turn_step_index = turn_step_index
  has_done_set_up_for_current_step = false

func go_to_next_turn_step():
  set_is_waiting(false)
  var next_turn_step_index = current_turn_step_index
  if next_turn_step_index == turn_step_order.size() - 1:
    next_turn_step_index = 0
  else:
    next_turn_step_index += 1
  _set_turn_step(next_turn_step_index)

func _get_current_set_up_step():
  return set_up_turn_step_order[current_turn_step_index]

func _go_to_next_set_up_turn_step():
  _set_turn_step(current_turn_step_index + 1)

func _finish_set_up():
  current_turn_step_index = 0
  has_done_game_start_up_initialization = true

func _process_set_up():
  if _get_current_set_up_step() == TurnStep.UpdatePlayerViewshed:
    emit_signal("update_player_viewshed")
    _go_to_next_set_up_turn_step()
  elif _get_current_set_up_step() == TurnStep.DetermineActorVisibility:
    emit_signal("determine_actor_visibility")
    _finish_set_up()

func _process(_delta):
  if not has_done_game_start_up_initialization:
    _process_set_up()
  elif has_done_game_start_up_initialization:
    if !has_done_set_up_for_current_step:
      _handle_set_up_for_current_step()
      return

    if get_current_step() == TurnStep.PlayerTurnCleanup:
      emit_signal("player_turn_cleanup")
      go_to_next_turn_step()
    elif get_current_step() == TurnStep.UpdatePlayerViewshed:
      emit_signal("update_player_viewshed")
      go_to_next_turn_step()
    elif get_current_step() == TurnStep.DetermineActorVisibility:
      emit_signal("determine_actor_visibility")
      go_to_next_turn_step()
    elif get_current_step() == TurnStep.EnemyTurn:
      emit_signal("enemy_turn")
      go_to_next_turn_step()
    elif get_current_step() == TurnStep.EnemyTurnCleanup:
      emit_signal("enemy_turn_cleanup")
      go_to_next_turn_step()
    elif get_current_step() == TurnStep.ActivateTraps:
      emit_signal("activate_traps")
      go_to_next_turn_step()

func _handle_set_up_for_current_step():
  if get_current_step() == TurnStep.PlayerTurn:
    emit_signal("player_turn_setup")
  has_done_set_up_for_current_step = true

func set_is_waiting(value: bool):
  is_waiting = value
extends Node


var a_star: AStar2D = AStar2D.new()

func _ready():
  MovementBlockerManager.connect("cell_blocked", self, "_handle_cell_blocked")
  MovementBlockerManager.connect("cell_cleared", self, "_handle_cell_cleared")
  MovementBlockerManager.connect("movement_blockers_updated", self, "_handle_world_map_updated")
  _handle_world_map_updated()


func _handle_cell_blocked(grid_position):
  var position_id = MapHelpers.calc_astar_id(grid_position, GameWorld.tile_map_rect)
  if a_star.has_point(position_id):
    a_star.remove_point(position_id)


func _handle_cell_cleared(grid_position):
  var position_id = MapHelpers.calc_astar_id(grid_position, GameWorld.tile_map_rect)
  if a_star.has_point(position_id):
    a_star.remove_point(position_id)


func _handle_world_map_updated():
  var rect_size = GameWorld.tile_map_rect.size
  var rect_position = GameWorld.tile_map_rect.position

  for x in range(rect_position.x, rect_position.x + rect_size.x):
    for y in range(rect_position.y, rect_position.y + rect_size.y):
      var grid_position = Vector2(x, y)
      if not grid_position in MovementBlockerManager.blockers:
        a_star.add_point(MapHelpers.calc_astar_id(grid_position, GameWorld.tile_map_rect), grid_position)
        _connect_point_to_grid(grid_position)


func _connect_point_to_grid(grid_position: Vector2):
  var tilemap_used_rect = GameWorld.tile_map_rect
  var position_id = MapHelpers.calc_astar_id(grid_position, tilemap_used_rect)

  if !a_star.has_point(position_id):
    return false

  for x in range(3):
    for y in range(3):
      var target = grid_position + Vector2(x - 1, y - 1)
      var target_id = MapHelpers.calc_astar_id(target, tilemap_used_rect)
      if grid_position == target or not a_star.has_point(target_id) or MovementBlockerManager.get_blocker(target)[0]:
        continue
      a_star.connect_points(position_id, target_id)


func get_closest_point_id(grid_position):
  var closest_point_id = a_star.get_closest_point(grid_position)
  if closest_point_id == -1:
    return null

  var closest_point = a_star.get_point_position(closest_point_id)
  var closest_point_distance = closest_point.distance_to(grid_position)

  if closest_point_distance != 1 and closest_point_distance != 0:
    return null
  return closest_point_id

func get_point_path(from: Vector2, to: Vector2):
  var from_id = MapHelpers.calc_astar_id(from, GameWorld.tile_map_rect)
  if from in MovementBlockerManager.blockers:
    a_star.add_point(from_id, from)
    _connect_point_to_grid(from)

  var to_id = get_closest_point_id(to)
  if from_id == null or to_id == null:
    return []
  # Okay, we've established that the closest point is accessible to `from`

  var path = a_star.get_point_path(from_id, to_id)
  if from in MovementBlockerManager.blockers:
    a_star.remove_point(from_id)
  return path


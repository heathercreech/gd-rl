extends Node

# The player movement requires special considerations, so the general AStarManager won't work.

var a_star: AStar2D = AStar2D.new()

func _ready():
  MovementBlockerManager.connect("cell_blocked", self, "_handle_cell_blocked")
  MovementBlockerManager.connect("cell_cleared", self, "_handle_cell_cleared")
  # GameWorld.connect("world_map_updated", self, "_handle_world_map_updated")
  VisibilityManager.connect("player_viewshed_updated", self, "_update_a_star_nodes")


func _handle_cell_blocked(grid_position):
  var position_id = MapHelpers.calc_astar_id(grid_position, GameWorld.tile_map_rect)
  if a_star.has_point(position_id):
    a_star.remove_point(position_id)


func _handle_cell_cleared(grid_position):
  var position_id = MapHelpers.calc_astar_id(grid_position, GameWorld.tile_map_rect)
  if a_star.has_point(position_id):
    a_star.remove_point(position_id)


func _handle_world_map_updated():
  var rect_size = GameWorld.tile_map_rect.size
  var rect_position = GameWorld.tile_map_rect.position

  for x in range(rect_position.x, rect_position.x + rect_size.x):
    for y in range(rect_position.y, rect_position.y + rect_size.y):
      var grid_position = Vector2(x, y)
      if not grid_position in MovementBlockerManager.blockers:
        a_star.add_point(MapHelpers.calc_astar_id(grid_position, GameWorld.tile_map_rect), grid_position)
        _connect_point_to_grid(grid_position)


func _connect_point_to_grid(grid_position: Vector2):
  var position_id = MapHelpers.calc_astar_id(grid_position, GameWorld.tile_map_rect)

  if !a_star.has_point(position_id):
    return false

  for x in range(3):
    for y in range(3):
      var target = grid_position + Vector2(x - 1, y - 1)
      var target_id = MapHelpers.calc_astar_id(target, GameWorld.tile_map_rect)
      if grid_position == target or not a_star.has_point(target_id) or MovementBlockerManager.get_blocker(target)[0]:
        continue
      a_star.connect_points(position_id, target_id)

func _update_a_star_nodes(visible_cells: Array):
  for cell in visible_cells:
    var cell_id = MapHelpers.calc_astar_id(cell, GameWorld.tile_map_rect)
    if not a_star.has_point(cell_id) and MovementBlockerManager.get_blocker(cell)[0] == null:
      a_star.add_point(cell_id, cell)
      _connect_point_to_grid(cell)

func get_point_path(from: Vector2, to: Vector2):
  var path = []
  var from_id = MapHelpers.calc_astar_id(from, GameWorld.tile_map_rect)
  var to_id = MapHelpers.calc_astar_id(to, GameWorld.tile_map_rect)

  a_star.add_point(from_id, from)
  _connect_point_to_grid(from)

  if a_star.has_point(from_id) and a_star.has_point(to_id):
    path = a_star.get_point_path(from_id, to_id)

    a_star.remove_point(from_id)
  return path

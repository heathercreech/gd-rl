extends TileMap

signal player_viewshed_updated(visible_cells)

var cells_between_debug = []

# Called when the node enters the scene tree for the first time.
func _ready():
  MovementBlockerManager.connect("movement_blockers_updated", self, "_handle_world_map_update")
  _handle_world_map_update()

func _handle_world_map_update():
  var rect_size = GameWorld.tile_map_rect.size
  var rect_position = GameWorld.tile_map_rect.position
  for x in range(rect_position.x, rect_position.x + rect_size.x):
    for y in range(rect_position.y, rect_position.y + rect_size.y):
      set_cell_visibility(Vector2(x, y), MapHelpers.VisibilityLevel.Unseen)


func set_cell_visibility(grid_position: Vector2, visibility_level: int):
  set_cellv(grid_position, visibility_level)

func get_cell_visibility(grid_position: Vector2):
  return get_cellv(grid_position)

func _get_cells_between(grid_position, target_position):
  var cells = []
  var delta_x = abs(target_position.x - grid_position.x)
  var sx
  if grid_position.x < target_position.x:
     sx = 1
  else:
    sx = -1
  var delta_y = -abs(target_position.y - grid_position.y)
  var sy
  if grid_position.y < target_position.y:
    sy = 1
  else:
    sy = -1
  var error = delta_x + delta_y

  var x = grid_position.x
  var y = grid_position.y
  while true:
    cells.append(Vector2(x, y))
    if x == target_position.x and y == target_position.y:
      break
    var error_2 = error * 2
    if error_2 >= delta_y:
      error += delta_y
      x += sx
    if error_2 <= delta_x:
      error += delta_x
      y += sy
  cells.append(target_position)
  return cells

func _get_cells_at_view_distance_perimeter(grid_position: Vector2, view_distance: int):
  var cells = [grid_position]
  var next_cell = Vector2(view_distance, 0)
  while next_cell.x > next_cell.y:
    cells.append(grid_position + next_cell)
    cells += _mirror_cell_to_full_circle(grid_position, next_cell)
    next_cell = _get_next_cell(next_cell.x, next_cell.y, view_distance)
  return cells

func get_visible_cells(grid_position: Vector2, view_distance: int):
  var perimeter_cells = _get_cells_at_view_distance_perimeter(grid_position, view_distance)
  var visible_cells = []
  for perimeter_cell in perimeter_cells:
    var cells_between = _get_cells_between(grid_position, perimeter_cell)
    for current_cell in cells_between:
      var is_cell_view_blocker = false
      var blocker_info = MovementBlockerManager.get_blocker(current_cell)
      if blocker_info[0] == Blockers.BlockerType.Tile:
        var tile_id = blocker_info[1]
        var tile_name = GameWorld.tile_set.tile_get_name(tile_id)
        var tile_properties = MapHelpers.tile_property_map[tile_name]
        if tile_properties.blocks_view:
          is_cell_view_blocker = true
      visible_cells.append(current_cell)
      if is_cell_view_blocker:
        break
  return visible_cells

func update_player_visibility(player_position: Vector2, view_distance: int):
  for cell in get_used_cells_by_id(MapHelpers.VisibilityLevel.Visible):
    set_cellv(cell, MapHelpers.VisibilityLevel.Seen)
  var visible_cells = get_visible_cells(player_position, view_distance)
  for cell in visible_cells:
    set_cellv(cell, MapHelpers.VisibilityLevel.Visible)
  emit_signal("player_viewshed_updated", visible_cells)

func _get_next_cell(x, y, radius):
  var radius_error = _calculate_radius_error(x, y, radius)
  var should_decrement_x = (2 * (radius_error + y) + x) > 0
  if should_decrement_x:
    return Vector2(x - 1, y + 1)
  else:
    return Vector2(x, y + 1)

func _mirror_cell_to_full_circle(center_position, delta_vector):
  var x = delta_vector.x
  var y = delta_vector.y
  return [
    center_position + Vector2(x, y),
    center_position + Vector2(-x, y),
    center_position + Vector2(x, -y),
    center_position + Vector2(-x, -y),
    center_position + Vector2(y, x),
    center_position + Vector2(-y, x),
    center_position + Vector2(y, -x),
    center_position + Vector2(-y, -x),
  ]


# Assuming a center point of 0, 0
func _calculate_radius_error(x, y, radius):
  return x * x + y * y - radius * radius

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#  pass

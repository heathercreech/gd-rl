extends Node

signal cell_blocked(cell_v)
signal cell_cleared(cell_v)
signal movement_blockers_updated()

var blockers: Dictionary = {}

func _ready():
  GameWorld.connect("world_map_updated", self, "_handle_world_map_update")
  _handle_world_map_update()

func _handle_world_map_update():
  for tile_position in GameWorld.get_used_cells():
    var tile_id = GameWorld.get_cell(tile_position.x, tile_position.y)
    var tile_name = GameWorld.tile_set.tile_get_name(tile_id)
    var tile_properties: MapHelpers.TileProperties = MapHelpers.tile_property_map[tile_name]
    if tile_properties.blocks_movement:
      blockers[tile_position] = [Blockers.BlockerType.Tile, tile_id]
  emit_signal('movement_blockers_updated')


func is_movement_blocked(grid_position: Vector2) -> bool:
  return blockers.has(grid_position)


func get_blocker(grid_position: Vector2) -> Array:
  if is_movement_blocked(grid_position):
    return blockers[grid_position]
  return [null]


func block_tile(grid_position: Vector2, blocking_root: Node):
  blockers[grid_position] = [Blockers.BlockerType.Entity, blocking_root]


func unblock_tile(grid_position: Vector2):
  if grid_position in blockers:
# warning-ignore:return_value_discarded
    blockers.erase(grid_position)

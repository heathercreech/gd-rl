extends Node
class_name ReactsToPlayerInViewshed

signal player_visible(player_position)

export(int) var view_distance = 8
onready var grid_mover: GridMover = get_node("../GridMover")
onready var player_grid_mover: GridMover = get_node("/root/DefaultScene/Player/GridMover")

var is_player_in_range = false
var has_determined_visibility
var last_visible_position
var cached_path = []

# Called when the node enters the scene tree for the first time.
func _ready():
# warning-ignore:return_value_discarded
  TurnManager.connect("player_turn_cleanup", self, 'update_viewshed')

func update_viewshed():
  is_player_in_range = player_grid_mover.grid_position.distance_to(grid_mover.grid_position) < view_distance
  if is_player_in_range:
    last_visible_position = player_grid_mover.grid_position
    var visible_cells = VisibilityManager.get_visible_cells(grid_mover.grid_position, view_distance)
    if player_grid_mover.grid_position in visible_cells:
      emit_signal("player_visible", player_grid_mover.grid_position)

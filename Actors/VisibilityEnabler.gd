extends Node


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

onready var scene_root_node: Node = get_node("..")
onready var grid_mover: GridMover = get_node("../GridMover")

# Called when the node enters the scene tree for the first time.
func _ready():
  TurnManager.connect("determine_actor_visibility", self, "determine_visibility")

func determine_visibility():
  var is_inside_player_viewshed = grid_mover.grid_position in VisibilityManager.get_used_cells_by_id(MapHelpers.VisibilityLevel.Visible)

  if is_inside_player_viewshed and not scene_root_node.visible:
    scene_root_node.show()
  elif scene_root_node.visible and not is_inside_player_viewshed:
    scene_root_node.hide()

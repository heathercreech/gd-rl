extends TileMap

signal world_map_updated

var Room = preload("res://Room.tscn")
var StartingArea = preload("res://StartingArea.tscn")

onready var background: ColorRect = $Background

var blockers: Dictionary = {}
var tile_map_rect: Rect2


func _ready():
# warning-ignore:return_value_discarded
  apply_tilemap_to(StartingArea.instance(), Vector2.ZERO)
  _clean_up_after_tilemap_update()


func _clean_up_after_tilemap_update():
  tile_map_rect = get_used_rect()

  $Background.rect_position = tile_map_rect.position * cell_size
  $Background.rect_size = tile_map_rect.size * cell_size

  emit_signal('world_map_updated')


# Applies an instance of a tilemap to a position on the world's tilemap, useful for keeping rooms defined in separate scenes
func apply_tilemap_to(source_tilemap: TileMap, grid_position: Vector2):
  for source_cell_position in source_tilemap.get_used_cells():
    var target_cell_position = grid_position + source_cell_position
    set_cellv(target_cell_position, source_tilemap.get_cellv(source_cell_position))


func grid_to_world(grid_position: Vector2) -> Vector2:
  return map_to_world(grid_position)


func world_to_grid(world_position: Vector2) -> Vector2:
  return world_to_map(world_position)

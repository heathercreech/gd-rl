extends CanvasLayer
class_name ActionBar

onready var item_container = $ActionBarItems
onready var item_buttons = item_container.get_children()


func _ready():
  for button in item_buttons:
    button.disabled = true


func register_action_with_slot(slot_index, target_node, function_name):
  if slot_index >= 0 and slot_index < item_buttons.size():
    var target_button = item_buttons[slot_index]
    _clear_button_actions(target_button)
    target_button.connect("pressed", target_node, function_name)
    target_button.disabled = false


func _clear_button_actions(target_button: Node):
  var connections = target_button.get_signal_connection_list("pressed")
  for connection in connections:
    target_button.disconnect(connection["signal"], connection.target, connection.method)
  connections = target_button.get_signal_connection_list("pressed")


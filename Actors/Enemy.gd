extends Node2D

onready var grid_mover: GridMover = $GridMover
onready var player_visibility: ReactsToPlayerInViewshed = $ReactsToPlayerInViewshed
onready var combat_stats: CombatStats = $CombatStats
onready var animation_player: AnimationPlayer = $AnimationPlayer
onready var health_bar: HealthBar = $HealthBar
var destination

# Called when the node enters the scene tree for the first time.
func _ready():
# warning-ignore:return_value_discarded
  player_visibility.connect("player_visible", self, "_on_player_visible")
# warning-ignore:return_value_discarded
  TurnManager.connect("enemy_turn", self, "_take_turn")
  destination = grid_mover.grid_position
  health_bar.update_fill_percent(1)

func _on_player_visible(player_position: Vector2):
  destination = player_position

func _take_turn():
  if destination != grid_mover.grid_position:
    var path = AStarManager.get_point_path(grid_mover.grid_position, destination)
    if path.size() > 1:
      grid_mover.move(path[1] - grid_mover.grid_position)
    elif path.size() == 1:
      grid_mover.move(destination - grid_mover.grid_position)

func _on_CombatStats_died():
  queue_free()

func _on_GridMover_blocked(blocked_grid_position):
  var blocker_info = MovementBlockerManager.get_blocker(blocked_grid_position)
  if blocker_info[0] == Blockers.BlockerType.Entity:
    var target_combat_stats: CombatStats = blocker_info[1].get_node("CombatStats")
    if target_combat_stats:
      target_combat_stats.take_damage(combat_stats.attack)


func _on_CombatStats_take_damage(_power, _health, _total_health):
  if animation_player:
    animation_player.play("Damaged")
  health_bar.update_fill_percent(combat_stats.health / combat_stats.health_total as float)

func scanned():
  health_bar.show()
  health_bar.update_fill_percent(combat_stats.health / combat_stats.health_total as float)

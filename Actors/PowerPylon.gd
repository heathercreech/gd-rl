extends Node2D
class_name PowerPylon

var current_state = PylonState.Dormant
onready var grid_mover: GridMover = $GridMover
var adjacent_pylon
const max_connection_range = 6
const activation_turn_wait_count = 1
var activation_counter = 0

enum PylonState {
  Dormant,
  Activating,
  Active
 }

# Called when the node enters the scene tree for the first time.
func _ready():
  TurnManager.connect("activate_traps", self, "_activate_pylons")


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#  pass

func _search_for_adjacent_pylon(direction):
  # Skipping 1 because that's the entity that activated this
  # TODO: Some sort of property that tells us an entity blocks a pylon connection
  for i in range(2, max_connection_range + 1):
    var blocker_info = MovementBlockerManager.get_blocker(grid_mover.grid_position + direction * i)
    if blocker_info[0] == Blockers.BlockerType.Entity:
      if blocker_info[1].get_class() == "PowerPylon":
        return blocker_info[1]
  return null

func _on_BlocksCell_bumped(from_position: Vector2):
  if current_state == PylonState.Activating:
    _disconnect_pylons()
    return

  var direction = grid_mover.grid_position.direction_to(from_position)
  var pylon_search_result = _search_for_adjacent_pylon(direction)
  if pylon_search_result != null and current_state == PylonState.Dormant:
    _connect_pylons(pylon_search_result)

func _connect_pylons(pylon):
  adjacent_pylon = pylon
  adjacent_pylon.adjacent_pylon = self
  adjacent_pylon.current_state = PylonState.Activating
  current_state = PylonState.Activating
  print("Connecting Pylons")

func _activate_pylons():
  if current_state != PylonState.Activating:
    return

  if activation_counter < activation_turn_wait_count:
    activation_counter += 1
  else:
    current_state = PylonState.Active
    adjacent_pylon = PylonState.Active
    activation_counter = 0
    print("Pylons Active")

func _disconnect_pylons():
  if adjacent_pylon != null:
    adjacent_pylon.current_state = PylonState.Dormant
    current_state = PylonState.Dormant
    adjacent_pylon.adjacent_pylon = null
    adjacent_pylon = null
    print("Disconnecting Pylons")

func get_class():
  return "PowerPylon"

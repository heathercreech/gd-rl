extends Control
class_name HealthBar

export (float) var percent_full = .25
onready var tween = $Tween
onready var bar = $ProgressBar
var tweened_percent = 0

func _ready():
  bar.value = percent_full * 100

func _process(_delta):
  bar.value = percent_full * 100

func update_fill_percent(updated_percent):

  tween.interpolate_property(self, "percent_full", percent_full, updated_percent, 0.2, Tween.TRANS_EXPO, Tween.EASE_OUT)
  if not tween.is_active():
    tween.start()

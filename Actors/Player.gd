extends Node2D

enum PlayerInputMode {
  Movement,
  Targeting,
  Delegated,
  Inventory
}

onready var combat_stats: CombatStats = $CombatStats
onready var grid_mover: GridMover = $GridMover
onready var movement = $PlayerMovement

var inventory = preload("res://UI/InventoryInterface.tscn")
var inventory_instance = null

var current_input_mode = PlayerInputMode.Movement

func _ready():
  TurnManager.connect("player_turn_setup", self, "_start_of_turn_set_up")

func _start_of_turn_set_up():
  current_input_mode = PlayerInputMode.Movement

func set_input_mode(new_input_mode):
  _disable_nodes_for_previous_input_mode(current_input_mode)
  current_input_mode = new_input_mode
  _enable_nodes_for_new_input_mode(new_input_mode)

func _enable_nodes_for_new_input_mode(new_input_mode):
  if new_input_mode == PlayerInputMode.Movement:
    movement.show()

func _disable_nodes_for_previous_input_mode(previous_input_mode):
  if previous_input_mode == PlayerInputMode.Movement:
    movement.hide()

func _on_GridMover_blocked(blocked_grid_position):
  var blocker = MovementBlockerManager.get_blocker(blocked_grid_position)
  match blocker[0]:
    Blockers.BlockerType.Entity:
      var target_combat_stats: CombatStats = blocker[1].get_node_or_null("CombatStats")
      if target_combat_stats:
        target_combat_stats.take_damage(combat_stats.attack)
    Blockers.BlockerType.Tile:
      print(GameWorld.tile_set.tile_get_name(blocker[1]))
    null:
      pass

func _input(event):
  if current_input_mode == PlayerInputMode.Inventory:
    _handle_inventory_input(event)
  elif TurnManager.get_current_step() == TurnManager.TurnStep.PlayerTurn:
    if event.is_action_pressed("open_inventory"):
      _show_inventory()

func _handle_inventory_input(event):
  if event.is_action_pressed("open_inventory"):
    _hide_inventory()

func _show_inventory():
  set_input_mode(PlayerInputMode.Inventory)
  inventory_instance = inventory.instance()
  get_node("/root/MenuMount").add_child(inventory_instance)

func _hide_inventory():
  inventory_instance.queue_free()
  inventory_instance = null
  set_input_mode(PlayerInputMode.Movement)

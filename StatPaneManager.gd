extends Node

onready var player = get_node("../GameWorld/Player")
onready var side_menu = get_node("../CanvasLayer/SideMenu")
var label: Label
var health_bar


func _ready():
  player.combat_stats.connect("take_damage", self, "_update_health_bar")
  label = side_menu.find_node("Label")
  health_bar = side_menu.find_node("HealthBar")
  health_bar.update_fill_percent(1)


func _update_health_bar(_damage, health, total_health):
  health_bar.update_fill_percent(health / total_health as float)

#func _process(delta):
#  pass
